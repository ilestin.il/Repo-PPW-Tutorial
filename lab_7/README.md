Notes untuk checklist Completion

1.1. Terdapat list yang berisi daftar mahasiswa fasilkom, yang dipanggil dari django model.

Pada fungsi index dalam views.py, terdapat variable mahasiswa_list yang menampung list yang diambil dari csuihelper.py, kemudian mahasiswa_list dimasukkan ke response untuk dirender

1.2. Buatlah tombol untuk dapat menambahkan list mahasiswa kedalam daftar teman (implementasikan menggunakan ajax).

Fungsi add friend akan dipanggil dengan setiap kali tombol "tambah sebagai teman" ditekan, akan disambungkan ke /add-friend/ yang memiliki fungsi add-friend. Kemudian dalam ajax akan diambil data nama dan npm siswa tersebut dan dimasukkan ke dalam model.

1.3 Mengimplentasikan validate_npm untuk mengecek apakah teman yang ingin dimasukkan sudah ada didalam daftar teman atau belum.

Saat add-friend, dalam fungsi add-friend akan dicek dengan filter apakah npmnya ada, jika ada maka ajax akan error dan memberi alert mahasiswa sudah jadi teman.

1.4 Membuat pagination (hint: salah satu data yang didapat dari kembalian api.cs.ui.ac.id adalah next dan previous yang bisa digunakan dalam membuat pagination)

Pada index ada var page yang melakukan GET pada page yang mengambil page dari html yang sedang dibuka, kemudian page itu dipergunakan untuk mengisi parameter page saat memanggil get_mahasiswa_list dari api csuihelper.py. Kemudian ada tombol next dan prev untuk increment dan decrement pge di html yang nantinya diambil page di view.

2.1 Terdapat list yang berisi daftar teman, data daftar teman didapat menggunakan ajax.

Pada html daftar-teman terdapat js untuk mengiterasi kembalian friend_list yang berisi semua objek dari model friend. Kemudian akan ditampilan nama, npm, serta tombol delete untuk setiap objek.

2.2 Buatlah tombol untuk dapat menghapus teman dari daftar teman (implementasikan menggunakan ajax).

Tombol delete untuk setiap mahasiswa tersambung pada fungsi delete-friend yang mendelete seluruh tag dnegan id friend yang berkoresponden.

3.1 Jika kalian belum melakukan konfigurasi untuk menampilkan Code Coverage di Gitlab maka lihat langkah Show Code Coverage in Gitlab di README.md

Sudah dilakukan

3.2 Pastikan Code Coverage kalian 100%

Terdapat tes fungsi, url, pagination, add dan delete friend, serta validasi untuk menjamin code coverage 100%